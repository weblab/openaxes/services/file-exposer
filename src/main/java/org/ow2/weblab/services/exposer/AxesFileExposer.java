/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2013 Cassidian, an EADS company
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.services.exposer;


import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.jws.WebService;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.content.api.ContentManager;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.factory.ExceptionFactory;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.util.ResourceUtil;
import org.ow2.weblab.core.model.Annotation;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.Image;
import org.ow2.weblab.core.model.MediaUnit;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.ow2.weblab.core.services.Analyser;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.analyser.ProcessArgs;
import org.ow2.weblab.core.services.analyser.ProcessReturn;
import org.ow2.weblab.rdf.Value;

/**
 * This service is used in conjunction with an Apache Tomcat or Apache server that is it self in charge of exposing the content.
 *
 * Its purpose is to annotate documents in input with an "exposition URL" i.e. an URL that can be used in a web browser to access this document.
 *
 * @author WebLab Team
 */
@WebService(endpointInterface = "org.ow2.weblab.core.services.Analyser")
public class AxesFileExposer implements Analyser {


	/**
	 * The instance of logger used inside AxesFileExposer
	 */
	protected final Log logger;


	/**
	 * The ContentManger that provides access to the files.
	 */
	protected final ContentManager contentManager;


	/**
	 * The local root.
	 */
	protected final String localRoot;


	/**
	 * The exposed root.
	 */
	protected final String exposedRoot;


	/**
	 * The service uri (might be null)
	 */
	protected final URI serviceUri;


	/**
	 * @param localRoot
	 *            The local root to be used when finding a file on the server and that has to be removed
	 * @param exposedRoot
	 *            The path that has to be used instead of local root
	 * @param serviceUri
	 *            The service URI to be used (or null)
	 */
	public AxesFileExposer(final String localRoot, final String exposedRoot, final String serviceUri) {
		this.logger = LogFactory.getLog(this.getClass());
		this.contentManager = ContentManager.getInstance();
		final File localRootFile = new File(localRoot);
		this.localRoot = this.toNormalisedPath(localRootFile.getAbsoluteFile());
		if (!localRootFile.isAbsolute()) {
			this.logger.warn("The local root " + localRoot + " is not an absolute path. Using " + this.localRoot);
		}
		this.exposedRoot = exposedRoot;
		if ((serviceUri == null) || serviceUri.isEmpty()) {
			this.serviceUri = null;
		} else {
			this.serviceUri = URI.create(serviceUri);
		}
	}


	@Override
	public ProcessReturn process(final ProcessArgs args) throws InvalidParameterException {
		this.logger.debug("Early start of process method.");

		final Resource resource = this.checkProcessArgs(args);

		this.logger.info("Start processing resource " + resource.getUri() + ".");

		final List<MediaUnit> mus = this.getMediaUnits(resource);

		this.logger.debug(mus.size() + " MediaUnit to process");

		for (final MediaUnit mu : mus) {
			this.exposeMediaUnit(mu);
		}

		this.logger.info("Resource " + resource.getUri() + " processed.");

		final ProcessReturn pr = new ProcessReturn();
		pr.setResource(resource);
		return pr;
	}


	protected WProcessingAnnotator exposeMediaUnit(final MediaUnit mu) {
		final WProcessingAnnotator wpaReader = new WProcessingAnnotator(mu);
		final Set<URI> contents = new HashSet<>();
		final Value<URI> normalisedContentValues = wpaReader.readNormalisedContent();
		if (normalisedContentValues.hasValue()) {
			contents.addAll(normalisedContentValues.getValues());
		}
		final Value<URI> nativeContentValues = wpaReader.readNativeContent();
		if (nativeContentValues.hasValue()) {
			contents.addAll(nativeContentValues.getValues());
		}
		if (contents.isEmpty()) {
			this.logger.warn("Neither native nor normalised content on " + mu.getUri());
			return null;
		}
		final Annotation expositionAnnotation = WebLabResourceFactory.createAndLinkAnnotation(mu);
		final WProcessingAnnotator wpaWriter;
		if (this.serviceUri == null) {
			wpaWriter = new WProcessingAnnotator(URI.create(mu.getUri()), expositionAnnotation);
		} else {
			wpaWriter = new WProcessingAnnotator(URI.create(mu.getUri()), expositionAnnotation);
			wpaWriter.startInnerAnnotatorOn(URI.create(expositionAnnotation.getUri()));
			wpaWriter.writeProducedBy(this.serviceUri);
			wpaWriter.endInnerAnnotator();
		}

		boolean anythingWritten = false;
		for (final URI path : contents) {
			final File file;
			try {
				file = this.contentManager.readContent(path, mu);
			} catch (final WebLabCheckedException wlce) {
				this.logger.warn("An error has occured for Content <" + path.toString() + "> in MediaUnit <" + mu.getUri() + ">.", wlce);
				continue;
			}
			wpaWriter.writeExposedAs(this.generatedExposedPath(file));
			anythingWritten = true;
		}
		if (!anythingWritten) {
			this.logger.warn("Nothing to add into MediaUnit " + mu.getUri() + ". Removing the created annotation.");
			mu.getAnnotation().remove(expositionAnnotation);
			return null;
		}
		return wpaWriter;
	}


	protected List<MediaUnit> getMediaUnits(final Resource resource) {
		final List<MediaUnit> mus = new ArrayList<MediaUnit>(ResourceUtil.getSelectedSubResources(resource, Image.class));
		if (resource instanceof Document) {
			final Document doc = (Document) resource;
			mus.add(doc);
			if (doc.isSetMediaUnit()) {
				mus.add(doc.getMediaUnit().get(0));
			}
		}
		return mus;
	}




	/**
	 * Method that tests that the ProcessArgs is correct regarding specification of AxesFileExposer
	 *
	 * @param args
	 *            The argument received by the process method
	 * @return The resource inside <code>args</code>
	 * @throws InvalidParameterException
	 *             If args or the resource it contains is <code>null</code>
	 */
	protected Resource checkProcessArgs(final ProcessArgs args) throws InvalidParameterException {
		if (args == null) {
			throw ExceptionFactory.createInvalidParameterException("ProcessArgs is null.");
		}
		final Resource resource = args.getResource();
		if (resource == null) {
			throw ExceptionFactory.createInvalidParameterException("Resource in ProcessArgs is null.");
		}

		return resource;
	}


	protected String generatedExposedPath(final File file) {
		String path = this.toNormalisedPath(file).replace(this.localRoot, "");

		final StringBuilder sb = new StringBuilder();
		sb.append(this.exposedRoot);
		final String[] splitted = path.split("/");
		for (final String element : splitted) {
			if (element.isEmpty()) {
				continue;
			}
			sb.append('/');
			try {
				sb.append(URLEncoder.encode(element, "UTF-8"));
			} catch (final UnsupportedEncodingException uee) {
				throw new RuntimeException(uee);
			}
		}
		return sb.toString().replace("+", "%20");
	}


	protected String toNormalisedPath(final File file) {
		String path;
		try {
			path = file.getCanonicalPath();
		} catch (final IOException ioe) {
			path = file.getAbsolutePath();
			this.logger.warn("Unable to get canonical path to " + path + ".", ioe);
		}

		path = FilenameUtils.normalize(path, true);
		return path;
	}

}