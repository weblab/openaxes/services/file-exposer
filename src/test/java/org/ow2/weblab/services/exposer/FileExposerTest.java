package org.ow2.weblab.services.exposer;

import org.junit.Assert;
import org.junit.Test;
import org.ow2.weblab.core.services.Analyser;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.FileSystemResource;

/**
 * Template for WebLab service test which uses Analyser interface
 *
 * @author WebLab Team
 */
public class FileExposerTest {


	@Test
	public void testLoadingCxf() throws Exception {
		XmlBeanFactory xmlBeanFactory = new XmlBeanFactory(new FileSystemResource("src/main/webapp/WEB-INF/cxf-servlet.xml"));
		Assert.assertEquals(1, xmlBeanFactory.getBeansOfType(Analyser.class).size());
		Assert.assertNotNull(xmlBeanFactory.getBean(Analyser.class));
	}


}
